#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

typedef struct {
    char maSinhVien[10];
    char ten[10];
    char soDienThoai[10];
} SinhVien;

void removeStdChar(char array[]) {
    if (strchr(array, '\n') == NULL) {
        while (fgetc(stdin) != '\n');
    }
}

void taoMenu() {
    printf("Menu cho phep nguoi dung lua chon\n");
    printf("1. Them moi sinh vien\n");
    printf("2. Hien thi danh sach sinh vien\n");
    printf("3. Luu danh sach sinh vien\n");
    printf("4. Doc danh sach sinh vien tu file\n");
    printf("5. Thoat chuong trinh\n");
    printf("Nhap lua chon cua ban\n");
}

void themMoiSinhVien(SinhVien mangSinhVien[], int i) {
    do {
        printf("Nhap ma sinh vien:\n");
        fgets(mangSinhVien[i].maSinhVien, sizeof(mangSinhVien[i].maSinhVien) * sizeof(char), stdin);
        removeStdChar(mangSinhVien[i].maSinhVien);
        mangSinhVien[i].maSinhVien[strlen(mangSinhVien[i].maSinhVien) - 1] = ' ';
        if (strlen(mangSinhVien[i].maSinhVien) != 6) {
            printf("Ma sinh vien phai gom 5 ki tu\n");
        }
    } while (strlen(mangSinhVien[i].maSinhVien) != 6);
    printf("Nhap ten sinh vien:\n");
    fgets(mangSinhVien[i].ten, sizeof(mangSinhVien[i].ten) * sizeof(char), stdin);
    removeStdChar(mangSinhVien[i].ten);
    mangSinhVien[i].ten[strlen(mangSinhVien[i].ten) - 1] = ' ';
    printf("Nhap so dien thoai sinh vien:\n");
    fgets(mangSinhVien[i].soDienThoai, sizeof(mangSinhVien[i].soDienThoai) * sizeof(char), stdin);
    removeStdChar(mangSinhVien[i].soDienThoai);
    mangSinhVien[i].soDienThoai[strlen(mangSinhVien[i].soDienThoai) - 1] = ' ';
}

void hienThiDanhSachSinhVien(SinhVien mangSinhVien[], int i) {
    printf("%-20s| %-20s| %-20s\n", "Ma Sinh Vien", "Ten", "So Dien Thoai");
    for (int j = 0; j < i; j++) {
        printf("%-20s| %-20s| %20s\n", mangSinhVien[j].maSinhVien, mangSinhVien[j].ten, mangSinhVien[j].soDienThoai);
    }
}

void luuThongTinSinhVien(FILE *fp, SinhVien mangSinhVien[], int i) {
    fprintf(fp, "%-20s|%-20s|%-20s\n", "Ma Sinh Vien", "Ten", "So Dien Thoai");
    for (int j = 0; j < i; j++) {
        fprintf(fp, "%-20s|%-20s|%-20s\n", mangSinhVien[j].maSinhVien, mangSinhVien[j].ten,
                mangSinhVien[j].soDienThoai);
    }
}

void docDanhSachSinhVien(FILE *fp, char fileSinhVien[]) {
    while (fgets(fileSinhVien,255* sizeof(char), fp) != NULL) {
        printf("%s", fileSinhVien);
    }
}

int main() {
    int luachon;
    int n = 0;
    SinhVien mangSinhVien[10];
    char fileSinhVien[255];
    FILE *fp;
    while (1) {
        taoMenu();
        scanf("%d", &luachon);
        getchar();
        switch (luachon) {
            case 1:
                if (n <= 10) {
                    themMoiSinhVien(mangSinhVien, n);
                    n++;
                } else {
                    printf("Danh sach sinh vien da day \n");
                    break;
                }
                break;
            case 2:
                hienThiDanhSachSinhVien(mangSinhVien, n);
                break;
            case 3:
                fp = fopen("../DanhSachSinhVien .txt", "w");
                if (fp != NULL) {
                    luuThongTinSinhVien(fp, mangSinhVien, n);
                    fclose(fp);
                }
                break;
            case 4:
                fp = fopen("../DanhSachSinhVien .txt", "r");
                docDanhSachSinhVien(fp, fileSinhVien);
                fclose(fp);
                break;
            case 5:
                return 0;
                break;
            default:
                printf("lua chon sai");
                break;
        }
    }
    return 0;
}